;
A PHP program to print out 1x1 to 12x12 multiplication table. Ideal for K12 educational sites.

Installation:

Installation is like with all normal drupal modules:
- Extract the 'k12_multiplier' folder from the tar ball to the modules directory for your website (typically sites/all/modules).
- Got to /admin/modules and enable module the module. 
- Clear your site cache by clicking on "Clear all caches" on /admin/config/development/performance

DRUSH:
- From your command line client, type: " drush dl k12_multiplier " to download module to your module directory (without quotes)
- Type " drush en k12_multiplier "
- Clear your cache by typing: "drush cc all"

Working:
- The working page is at YOURSITE/k12-multiplier